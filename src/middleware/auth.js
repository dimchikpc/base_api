import store from '../store'
import router from '../router'

export default function auth(to, from, next) {
  if(!store.getters.isAuthenticated) {
    return router.push({name: 'login'})
  }
  return next();
}
