import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const API_URL = 'http://localhost:8000/api/auth'
const AXIOS_CONFIG = {
  headers: {
    "Access-Control-Allow-Origin": "*"
  }
}

export default new Vuex.Store({
  state: {
    token: localStorage.getItem('token') || null,
    status: '',
    user: {},
    usersList: [],
    rolesList: [],
  },
  mutations: {
    AUTH_REQUEST: (state) => {
      state.status = 'loading'
    },
    AUTH_SUCCESS: (state, token) => {
      state.status = 'success'
      state.token = token
    },
    AUTH_ERROR: (state) => {
      state.status = 'error'
    },
    DESTROY_STATE_TOKEN: (state) => {
      state.token = null
    },
    SET_USER: (state, user) => {
      state.user = user
    },
    SET_USERS_LIST: (state, users) => {
      state.usersList = users
    },
    SET_ROLES_LIST: (state, roles) => {
      state.rolesList = roles
    }
  },
  getters: {
    isAuthenticated: state => !!state.token,
    authStatus: state => state.status,
    getUser: state => state.user,
    getUsersList: state => state.usersList,
    getRolesList: state => state.rolesList,
  },
  actions: {
    LOGIN: ({commit, dispatch}, user) => {
      return new Promise((resolve, reject) => {
        commit('AUTH_REQUEST')
        axios.post(`${API_URL}/login`, user, AXIOS_CONFIG)
          .then(response => {
            console.log(response)
            const token = response.data.access_token
            localStorage.setItem('token', token)
            commit('AUTH_SUCCESS', token)
            dispatch('USER_REQUEST')
            resolve(response)
          })
          .catch(error => {
            commit('AUTH_ERROR', error)
            localStorage.removeItem('token')
            reject(error)
          })
      })
    },
    USER_REQUEST: (context) => {
      if(context.state.token){
        return new Promise((resolve, reject) => {
          axios.post(`${API_URL}/me`, [], {
            'headers' : { 'X-Authorization': `bearer ${context.state.token}` }
          })
            .then(response => {
              console.log(response)
              context.commit('SET_USER', response.data)
              resolve(response)
            })
            .catch(error => {
              localStorage.removeItem('token')
              context.dispatch('DESTROY_TOKEN')
              reject(error)
            })
        })
      }
    },
    DESTROY_TOKEN: (context) => {
      if(context.getters.isAuthenticated){
        axios.post(`${API_URL}/logout`, [], {
          'headers' : { 'X-Authorization': `bearer ${context.state.token}` }
        })
          .then(response => {
            localStorage.removeItem('token')
            context.commit('DESTROY_STATE_TOKEN')
          })
          .catch(error => {
            localStorage.removeItem('token')
            context.commit('DESTROY_STATE_TOKEN')
          })
      }
    },
    GET_USERS: (context) => {
      if(context.getters.isAuthenticated){
        axios.get(`${API_URL}/users`, {
          'headers' : { 'X-Authorization': `bearer ${context.state.token}` }
        })
          .then(response => {
            context.commit('SET_USERS_LIST', response.data) //get-roles
          })
          .catch(error => {

          })
      }
    },
    GET_ROLES: (context) => {
      if(context.getters.isAuthenticated){
        axios.get(`${API_URL}/get-roles`, {
          'headers' : { 'X-Authorization': `bearer ${context.state.token}` }
        })
          .then(response => {
            context.commit('SET_ROLES_LIST', response.data)
          })
          .catch(error => {

          })
      }
    },
    UPDATE_USER: (context, data) => {
      if(context.getters.isAuthenticated) {
        return new Promise((resolve, reject) => {
          axios.put(`${API_URL}/users/${data.user_id}`, data.user, {
            'headers' : { 'X-Authorization': `bearer ${context.state.token}` }
          })
            .then(response => {
              console.log(response)
              context.dispatch('GET_USERS')
            })
            .catch(error => {
              reject(error)
            })
        })
      }
    },
    CREATE_USER: (context, data) => {
      if(context.getters.isAuthenticated) {
        return new Promise((resolve, reject) => {
          axios.post(`${API_URL}/users`, data.user, {
            'headers' : { 'X-Authorization': `bearer ${context.state.token}` }
          })
            .then(response => {
              console.log(response)
              context.dispatch('GET_USERS')
            })
            .catch(error => {
              reject(error)
            })
        })
      }
    },
    DELETE_USER: (context, id) => {
      if(context.getters.isAuthenticated) {
        return new Promise((resolve, reject) => {
          axios.delete(`${API_URL}/users/${id}`, {
            'headers' : { 'X-Authorization': `bearer ${context.state.token}` }
          })
            .then(response => {
              console.log(response)
              context.dispatch('GET_USERS')
            })
            .catch(error => {
              reject(error)
            })
        })
      }
    }
  }
})
